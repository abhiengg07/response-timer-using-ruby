#!usr/bin/env ruby

require 'optparse'
require 'net/http'
require 'uri'
require 'benchmark'

options = {}

opt_parser = OptionParser.new do |opt|
  opt.banner = "Usage: opt_parser COMMAND [OPTIONS]"
  opt.separator  ""
  opt.separator  "Commands"
  opt.separator  "     URL: tell the code your web address"
  opt.separator  ""
  opt.separator  "Options"

  opt.on("-a","--address URL","tell the code your web address") do |url|
    options[:url] = url
  end

  opt.on("-h","--help","help") do
    puts opt_parser
  end
end

opt_parser.parse!
url = options[:url] || 'https://gitlab.com'

case ARGV[0]
when "response_time"
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  req = Net::HTTP::Get.new(uri.request_uri)
  http.use_ssl = true
  time = Benchmark.realtime do
  response = http.request(req)
  end
  puts  "Response time to #{url} is #{time*1000} ms"
else
  puts opt_parser
end
