A ruby program to parse the console for web address and log the response time of the server using HTTP GET request. I used the benchmark package to time the response.  If you do not provide any web address then the default website is https://gitlab.com .

Usage:

```ruby
  ruby Response_timer_http.rb response_time -a "Your web address here"
```
